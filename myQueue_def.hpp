#ifndef MYQUEUE_DEF_H_INCLUDED
#define MYQUEUE_DEF_H_INCLUDED
#include<cstddef>
#include<deque>
#include<mutex>
#include<shared_mutex>
#include<thread>
#include <algorithm>
#include <iostream>
#include <compare>
#include <type_traits>
#include <unordered_set>
#include <functional>
template<typename T, class Container = std::deque<T>> 
class myQueue
{
    public:
        myQueue();
        myQueue(const myQueue& tmp);
        myQueue(myQueue&& tmp);
        myQueue( const Container& cont );
        myQueue( Container&& cont );
        ~myQueue();
        myQueue& operator= (const myQueue& copy);
        myQueue& operator= (myQueue&& moved);
        T& front();
        const T& front() const;
        T& back();
        const T& back() const;
        bool empty() const;
        size_t size() const; 
        void push(const T& elem); 
        void push(T&& elem);
        template<typename... Args>
        void emplace(Args&&... args);
        void pop();
        void swap(myQueue& swapped);
        auto operator<=>(const myQueue& myq) const;
        auto operator==(const myQueue& myq) const;
    private:
        myQueue(myQueue&& tmp, const std::scoped_lock<std::shared_mutex>&);
        myQueue(const myQueue& tmp, const std::scoped_lock<std::shared_mutex>&);
        mutable std::shared_mutex m;
        Container underlaying_container;
};

#endif // MYQUEUE_DEF_H_INCLUDED