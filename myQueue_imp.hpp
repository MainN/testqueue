#ifndef MYQUEUE_IMP_H_INCLUDED
#define MYQUEUE_IMP_H_INCLUDED
#include "myQueue_def.hpp"


template<typename T, typename Container>
myQueue<T, Container>::myQueue() :  underlaying_container{Container {}} {}

template<typename T, typename Container>
myQueue<T, Container>::myQueue(const myQueue& tmp) : myQueue(tmp,std::scoped_lock<std::shared_mutex>(tmp.m))  {}

template<typename T, typename Container>
myQueue<T, Container>::myQueue(myQueue&& tmp): myQueue(std::move(tmp),std::scoped_lock<std::shared_mutex>(tmp.m)) {}

//Приватные конструкторы с захватом мьютекса для предотвращения состояния гонки
template<typename T, typename Container>
myQueue<T, Container>::myQueue(const myQueue& tmp, const std::scoped_lock<std::shared_mutex>&):  underlaying_container{tmp.underlaying_container} {}
template<typename T, typename Container>
myQueue<T, Container>::myQueue(myQueue&& tmp, const std::scoped_lock<std::shared_mutex>&):  underlaying_container{std::move(tmp.underlaying_container)} {}

template<typename T, typename Container>
myQueue<T, Container>::myQueue(const Container& cont ): underlaying_container{cont} {}

template<typename T, typename Container>
myQueue<T, Container>::myQueue(Container&& cont ): underlaying_container{std::move(cont)} {}

template <typename T, typename Container>
myQueue<T, Container>::~myQueue()
{
    
}



template<typename T, typename Container>
myQueue<T, Container>& myQueue<T, Container>::operator=(const myQueue<T, Container>& copy)
{
    std::scoped_lock lock(m,copy.m);
    if (this!= &copy)
    {
       underlaying_container = copy.underlaying_container;
    }
    return *this;
}
template<typename T, typename Container>
myQueue<T, Container>& myQueue<T, Container>::operator=(myQueue<T, Container>&& moved)
{
    std::scoped_lock lock(m,moved.m);
    if (this!= &moved)
    {
        underlaying_container  = std::move(moved.underlaying_container);
    }
    return *this;
}



template<typename T, typename Container>
T& myQueue<T, Container>::front()
{
    std::scoped_lock lock(m);
    return underlaying_container.front();
}
template<typename T, typename Container>
const T& myQueue<T, Container>::front() const
{
    std::shared_lock lock(m);
    return underlaying_container.front();
}



template<typename T, typename Container>
bool myQueue<T, Container>::empty() const 
{
    std::shared_lock lock(m);
    return underlaying_container.empty();
}
template<typename T, typename Container>
size_t myQueue<T, Container>::size() const
{
    std::shared_lock lock(m);
    return underlaying_container.size();
}


template<typename T, typename Container>
T& myQueue<T, Container>::back()
{
    std::scoped_lock lock(m);
    return underlaying_container.back();
}
template<typename T, typename Container>
const T& myQueue<T, Container>::back() const
{
    std::shared_lock lock(m);
    return underlaying_container.back();
}



template <typename T, typename Container>
void myQueue<T, Container>::push(const T& elem)
{
    std::scoped_lock lock(m);
    if (std::ranges::find(underlaying_container, elem) == underlaying_container.end())
    {
        underlaying_container.push_back(elem);    
    }
    
}
template <typename T, typename Container>
void myQueue<T, Container>::push(T&& elem)
{
    std::scoped_lock lock(m);
    if (std::ranges::find(underlaying_container, elem) == underlaying_container.end())
    {
        underlaying_container.push_back(std::move(elem));    
    }
}



template <typename T, typename Container>
template <typename... Args>
void myQueue<T, Container>::
emplace(Args&&... args)
{
    std::scoped_lock lock(m);
    T tmp;
    tmp = T(std::forward<Args>(args)...);
    if (std::ranges::find(underlaying_container, tmp) == underlaying_container.end())
    {
        underlaying_container.emplace_back(std::move(tmp));
    }

}



template <typename T, typename Container>
void myQueue<T, Container>::pop()
{
    std::scoped_lock lock(m);
    underlaying_container.pop_front();
}
template <typename T, typename Container>
void myQueue<T, Container>::swap(myQueue& swapped)
{
    std::scoped_lock lock(m,swapped.m);
    std::swap(underlaying_container,swapped.underlaying_container);
}
template <typename T, typename Container>
auto myQueue<T, Container>::operator<=>(const myQueue<T, Container>& myq) const
{
    std::scoped_lock lock(m,myq.m);
    return underlaying_container <=> myq.underlaying_container;
}
template <typename T, typename Container>
auto myQueue<T, Container>::operator==( const myQueue<T, Container>& myq) const 
{
    std::scoped_lock lock(m,myq.m);
    return underlaying_container == myq.underlaying_container;
}

#endif //MYQUEUE_IMP_H_INCLUDED