#include <functional>
#include <iostream>
#include <utility>
#include <queue>
#include <vector>
#include <deque>
#include "myQueue_imp.hpp"
#include <thread>
#include <chrono>
#include <unordered_set>
#include <list>
#include <condition_variable>
#include <ranges>
std::mutex g_mutex;
std::condition_variable g_cv;
bool g_ready = false;

int main()
{
     myQueue<int> defaulQueue {}; 
     auto producer = [&defaulQueue](int data = 0) {
     for (;;) {
         std::unique_lock<std::mutex> ul(g_mutex);
         g_ready = true;
         defaulQueue.push(data);
         std::cout << data<<" pushed by "<< std::this_thread::get_id()<<std::endl;
         ++data;
         ul.unlock();
         g_cv.notify_one();
         if (data == 10)
         return;
         ul.lock();
         g_cv.wait(ul, []() { return g_ready == false; });
        
    }};
    auto consumer = [&defaulQueue](int data = 0) {
    for (;;) {
        std::unique_lock<std::mutex> ul(g_mutex);
        g_cv.wait(ul, []() { return g_ready; });
        data = defaulQueue.front();
        defaulQueue.pop();
        std::cout<< data<<" received by "  << std::this_thread::get_id()<<std::endl;
        if (data == 9)
        return;
        g_ready = false;
        ul.unlock();
        g_cv.notify_one(); 
        
    }};
    
     std::thread thread1(producer);
     std::thread thread2(consumer); 
     thread1.join();
     thread2.join();
     //Текущий размер строки
     std::cout << std::endl<<defaulQueue.size()<<std::endl;
     //Добавляем 10 раз одинаковый элемент и пытаемся 10 раз сконструировать объект на месте
     for(const auto& elem: std::views::iota(0,10))
     {
        defaulQueue.push(0);
        defaulQueue.emplace(0);  
     }
     //Очередь содержит только один элемент
     std::cout << std::endl<<defaulQueue.size()<<std::endl;
}; 
    


